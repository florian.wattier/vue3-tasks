import Api from './api';
import Task from '../interfaces/Task'
const resource = 'task';

export default {
  getTasks(params = {}) {
    return Api().get(`${resource}`, { params: params });
  },

  addTask(task: Task) {
    return Api().post(`${resource}/`, task);
  },

  updateTask(task: Task) {
    return Api().patch(`${resource}/${task._id}`, task);
  },

  removeTask(id: string) {
    return Api().delete(`${resource}/${id}`);
  }
};
