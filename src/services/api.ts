import axios from 'axios'
export default () => {
  const instance = axios.create({
    baseURL: 'http://91.175.246.3/'
  });

  return instance;
};
