import { createApp } from 'vue'
import './style.scss'
import App from './App.vue'

import Notifications from '@kyvg/vue3-notification'
import { Modal } from 'bootstrap'

import Vue3EasyDataTable from 'vue3-easy-data-table';
import 'vue3-easy-data-table/dist/style.css';

const app = createApp(App);

app
.component('modal', Modal)
.use(Notifications)
.component('EasyDataTable', Vue3EasyDataTable)
.mount('#app')