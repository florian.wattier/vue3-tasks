export default interface Task {
    _id: string,
    date: any,
    name: string,
    description: string
  }