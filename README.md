# Vue3 tasks

## Getting started

## Pré-requis
Node > 17 + npm

## Installation
Cloner le projet, se placer à la racine du projet

Installer les dépendances avec `npm install`

Builder le projet avec `npm run build`

Placer le contenu du dossier `dist` sur un serveur (nginx, apache, ...)

## Usage
 En tant que client, je veux une page web sur laquelle je peux voir une liste de tâches définies par date, nom et description
 
 En tant que client je veux pouvoir modifier ou supprimer une tâche directement depuis le tableau
 
 En tant qu’utilisateur, si une tache a lieu aujourd’hui, je veux que le système m’alerte

## Support
Florian Wattier
fwattier@live.fr

## Roadmap
Dans le futur, on peut imaginer la possibilité d'ajouter des tâches, un outil d'export sur un agenda, des notifications push...
Techniquement on pourra ajouter un router et faire de petits composants réutilisables.